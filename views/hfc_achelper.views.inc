<?php

/**
 * @file
 * Views handlers for hfc_achelper.module.
 */

/**
 * Implements hook_views_data_alter().
 */
function hfc_achelper_views_data_alter(&$data) {
  // Add a dummy field on field_data_body that will
  // return the path to the node if the body field is not empty.
  $data['field_data_body']['body_link'] = array(
    'group' => t('Content'),
    'title' => t('Body link'),
    'help' => t('Return a link path if the body is not empty'),
    'real field' => 'body_value',
    'field' => array(
      'handler' => 'hfc_achelper_handler_body_link',
      'click sortable' => FALSE,
    ),
  );
}

/**
 * Return a path if the body is not empty.
 *
 * @ingroup views_filter_handlers
 */
class hfc_achelper_handler_body_link extends views_handler_field {
  /**
  * Render the field.
  */
  public function render($values) {
    $value = $this->get_value($values);
    return !empty($value) ? url('node/' . $values->nid, array('absolute' => TRUE)) : NULL;
  }
}
